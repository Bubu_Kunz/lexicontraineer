package com.example.yzubritskiy.lexicontrainer;


public class DB_Helper {
    public static final String DB_NAME = "lexicon_trainer_db";
    public static final int DB_VERSION = 1;
    public static final String DB_TABLE_UNITS = "DB_TABLE_UNIT";
    public static final String DB_TABLE_SUBUNITS = "DB_TABLE_SUBUNITS";
    public static final String DB_TABLE_CARDS_TEXTS = "DB_TABLE_CARDS_TEXTS";
    public static final String DB_TABLE_CARDS = "DB_TABLE_CARDS";
    public static final String DB_TABLE_WORDS = "DB_TABLE_TRANSLATES";
    public static final String DB_TABLE_TRANSLATES = "DB_TABLE_TRANSLATES";
    public static final String DB_TABLE_TEXTS = "DB_TABLE_TEXTS";

    public static final String DB_TABLE_UNIT_COLUMN_ID = "DB_TABLE_UNIT_COLUMN_ID";
    public static final String DB_TABLE_UNIT_COLUMN_NAME = "DB_TABLE_UNIT_COLUMN_NAME";
    public static final String DB_TABLE_UNIT_COLUMN_DATE_OF_CREATING = "DB_TABLE_UNIT_COLUMN_DATE_OF_CREATING";

    public static final String DB_TABLE_SUBUNITS_ID = "DB_TABLE_SUBUNITS_ID";
    public static final String DB_TABLE_SUBUNITS_NAME = "DB_TABLE_SUBUNITS_NAME";
    public static final String DB_TABLE_SUBUNITS_DATE_OF_CREATING = "DB_TABLE_SUBUNITS_DATE_OF_CREATING";
    public static final String DB_TABLE_SUBUNITS_UNIT_ID = "DB_TABLE_SUBUNITS_UNIT_ID";

    public static final String DB_TABLE_CARDS_TEXTS_COLUMN_TEXT_ID = "CARDS_TEXTS_COLUMN_TEXT_ID";
    public static final String DB_TABLE_CARDS_TEXTS_COLUMN_CARD_ID = "CARDS_TEXTS_COLUMN_CARD_ID";

    public static final String DB_TABLE_CARDS_ID = "DB_TABLE_CARDS_ID";
    public static final String DB_TABLE_CARDS_WORD_ID = "DB_TABLE_CARDS_WORD_ID";
    public static final String DB_TABLE_CARDS_DATE_OF_CREATING = "DB_TABLE_CARDS_DATE_OF_CREATING";
    public static final String DB_TABLE_CARDS_DATE_OF_LAST_RIGHT_ANSWER = "DB_TABLE_CARDS_DATE_OF_LAST_RIGHT_ANSWER";
    public static final String DB_TABLE_CARDS_SUBUNIT_ID = "DB_TABLE_CARDS_SUBUNIT_ID";
    public static final String DB_TABLE_CARDS_SYNONYM_1_ID = "DB_TABLE_CARDS_SYNONYM_1_ID";
    public static final String DB_TABLE_CARDS_SYNONYM_2_ID = "DB_TABLE_CARDS_SYNONYM_2_ID";
    public static final String DB_TABLE_CARDS_SYNONYM_3_ID = "DB_TABLE_CARDS_SYNONYM_3_ID";
    public static final String DB_TABLE_CARDS_SYNONYM_4_ID = "DB_TABLE_CARDS_SYNONYM_4_ID";
    public static final String DB_TABLE_CARDS_SYNONYM_5_ID = "DB_TABLE_CARDS_SYNONYM_5_ID";
    public static final String DB_TABLE_CARDS_SYNONYM_6_ID = "DB_TABLE_CARDS_SYNONYM_6_ID";
    public static final String DB_TABLE_CARDS_SYNONYM_7_ID = "DB_TABLE_CARDS_SYNONYM_7_ID";
    public static final String DB_TABLE_CARDS_SYNONYM_8_ID = "DB_TABLE_CARDS_SYNONYM_8_ID";
    public static final String DB_TABLE_CARDS_SYNONYM_9_ID = "DB_TABLE_CARDS_SYNONYM_9_ID";
    public static final String DB_TABLE_CARDS_SYNONYM_10_ID = "DB_TABLE_CARDS_SYNONYM_10_ID";

    public static final String DB_TABLE_WORDS_ID = "DB_TABLE_WORDS_ID";
    public static final String DB_TABLE_WORDS_EXPRESSION = "DB_TABLE_WORDS_EXPRESSION";
    public static final String DB_TABLE_WORDS_PATH_TO_AUDIO = "DB_TABLE_WORDS_PATH_TO_AUDIO";

    public static final String DB_TABLE_TRANSLATES_ID = "DB_TABLE_TRANSLATES_ID";
    public static final String DB_TABLE_TRANSLATES_EXPRESSION = "DB_TABLE_TRANSLATES_EXPRESSION";
    public static final String DB_TABLE_TRANSLATES_DESCRIPTION = "DB_TABLE_TRANSLATES_DESCRIPTION";
    public static final String DB_TABLE_TRANSLATES_PATH_TO_PICTURE = "DB_TABLE_TRANSLATES_PATH_TO_PICTURE";

    public static final String DB_TABLE_TEXTS_ID = "DB_TABLE_TRANSLATES_ID";
    public static final String DB_TABLE_TEXTS_PATH_TO_FILE = "DB_TABLE_TEXTS_PATH_TO_FILE";
    public static final String DB_TABLE_TEXTS_PATH_TO_AUDIO = "DB_TABLE_TEXTS_PATH_TO_AUDIO";
    public static final String DB_TABLE_TRANSLATES_UNIT_ID = "DB_TABLE_TRANSLATES_UNIT_ID";

    private static final String DB_CREATE_TABLE_UNITS = "create table "
            + DB_TABLE_UNITS + "(" + DB_TABLE_UNIT_COLUMN_ID
            + " integer primary key autoincrement, "
            + DB_TABLE_UNIT_COLUMN_NAME + " text, "
            + DB_TABLE_UNIT_COLUMN_DATE_OF_CREATING + " integer"+");";

    private static final String DB_CREATE_TABLE_SUBUNITS = "create table "
            + DB_TABLE_SUBUNITS + "(" + DB_TABLE_SUBUNITS_ID
            + " integer primary key autoincrement, "
            + DB_TABLE_SUBUNITS_NAME + " text, "
            + DB_TABLE_SUBUNITS_DATE_OF_CREATING + " integer, "
            + DB_TABLE_SUBUNITS_UNIT_ID +" integer"+");";

    private static final String DB_CREATE_TABLE_CARDS_TEXTS = "create table "
            + DB_TABLE_CARDS_TEXTS + "(" + DB_TABLE_CARDS_TEXTS_COLUMN_TEXT_ID
            + " integer, "
            + DB_TABLE_CARDS_TEXTS_COLUMN_CARD_ID+" integer"+");";

    private static final String DB_CREATE_TABLE_CARDS = "create table "
            + DB_TABLE_CARDS + "(" + DB_TABLE_CARDS_ID
            + " integer primary key autoincrement, "
            + DB_TABLE_CARDS_WORD_ID + " integer, "
            + DB_TABLE_CARDS_DATE_OF_CREATING + " integer, "
            + DB_TABLE_CARDS_DATE_OF_LAST_RIGHT_ANSWER + " integer, "
            + DB_TABLE_CARDS_SUBUNIT_ID + " integer, "
            + DB_TABLE_CARDS_SYNONYM_1_ID + " integer, "
            + DB_TABLE_CARDS_SYNONYM_2_ID + " integer, "
            + DB_TABLE_CARDS_SYNONYM_3_ID + " integer, "
            + DB_TABLE_CARDS_SYNONYM_4_ID + " integer, "
            + DB_TABLE_CARDS_SYNONYM_5_ID + " integer, "
            + DB_TABLE_CARDS_SYNONYM_6_ID + " integer, "
            + DB_TABLE_CARDS_SYNONYM_7_ID + " integer, "
            + DB_TABLE_CARDS_SYNONYM_8_ID + " integer, "
            + DB_TABLE_CARDS_SYNONYM_9_ID + " integer, "
            + DB_TABLE_CARDS_SYNONYM_10_ID +" integer"+");";

    private static final String DB_CREATE_TABLE_WORDS = "create table "
            + DB_TABLE_WORDS + "(" + DB_TABLE_WORDS_ID
            + " integer primary key autoincrement, "
            + DB_TABLE_WORDS_EXPRESSION + " text, "
            + DB_TABLE_WORDS_PATH_TO_AUDIO +" text"+");";

    private static final String DB_CREATE_TABLE_TRANSLATES = "create table "
            + DB_TABLE_TRANSLATES + "(" + DB_TABLE_TRANSLATES_ID
            + " integer primary key autoincrement, "
            + DB_TABLE_TRANSLATES_EXPRESSION + " text, "
            + DB_TABLE_TRANSLATES_DESCRIPTION + " text, "
            + DB_TABLE_TRANSLATES_PATH_TO_PICTURE +" text"+");";

    private static final String DB_CREATE_TABLE_TEXTS = "create table "
            + DB_TABLE_TEXTS + "(" + DB_TABLE_TEXTS_ID
            + " integer primary key autoincrement, "
            + DB_TABLE_TEXTS_PATH_TO_FILE + " text, "
            + DB_TABLE_TEXTS_PATH_TO_AUDIO + " text, "
            + DB_TABLE_TRANSLATES_UNIT_ID +" integer"+");";

}
